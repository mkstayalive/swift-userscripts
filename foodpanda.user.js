// ==UserScript==
// @name        Foodpanda Google Maps Directions
// @namespace   https://restaurant.foodpanda.in
// @description Shows Foodpanda directions link below the address
// @include     http://restaurant.foodpanda.in/app*
// @include     https://restaurant.foodpanda.in/app*
// @require     https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js
// @version     1.1
// @grant       none
// ==/UserScript==

function addXMLRequestCallback(callback) {
    var oldSend, i;
    if (XMLHttpRequest.callbacks) {
        // we've already overridden send() so just add the callback
        XMLHttpRequest.callbacks.push(callback);
    } else {
        // create a callback queue
        XMLHttpRequest.callbacks = [callback];
        // store the native send()
        oldSend = XMLHttpRequest.prototype.send;
        // override the native send()
        XMLHttpRequest.prototype.send = function(){
            for (i = 0; i < XMLHttpRequest.callbacks.length; i++) {
                XMLHttpRequest.callbacks[i](this); 
            }
            // call the native send()
            oldSend.apply(this, arguments);
        };
    }
}

function getOrdersData() {
    return JSON.parse(localStorage.getItem('orders_data'));
}

function setOrdersData(ordersData) {
    localStorage.setItem('orders_data', JSON.stringify(ordersData));
}

addXMLRequestCallback(function(xhr) {
    xhr.onloadend = function() {
        if (xhr.responseURL.startsWith("https://restaurant.foodpanda.in/api/v2/orders-sync")) {
            var data = JSON.parse(xhr.response);
            if (!data || $.isEmptyObject(data.items)) return;
            var ordersData = getOrdersData();
            if (!ordersData) {
                setOrdersData({});
            }
            $(data.items).each(function(i, item) {
                ordersData[item.id] = item;
            });
            setOrdersData(ordersData);
        }
    };
});

$(function() {
    if (window.top != window.self)  //-- Don't run on frames or iframes
        return;

    function showDeliveryLocationDelayed(retryCount) {
        var $li = $('.table-view-container li.order-card.current');
        var parts = $li.attr('href').split('/');
        var id = parts[parts.length - 1];
        console.log('selected id: ' + id);
        var $target = $('.panel.order-customer .customer-body');
        var ordersData = getOrdersData();
        if (!ordersData[id] || !$target.length) {
            console.log('ordersData[id] = ' + ordersData[id] + ', $target.length = ' + $target.length);
            if (retryCount) {
                setTimeout(function() {
                    showDeliveryLocationDelayed(retryCount - 1);
                }, 100);
            } else if ($target.length) {
                $('.google-maps-directions').remove();
                $target.append('<div class="google-maps-directions"><h5>Location not available</h5></div>');
            }
            return;
        }
        $('.google-maps-directions').remove();
        // https://www.google.com/maps/dir/Eaternet+BTM,+VP+Road,+Dollar+Scheme+Colony,+Bengaluru,+Karnataka,+India/12.910966,77.633385
        var directionsUrl = 'https://www.google.com/maps/dir/Eaternet+BTM,+VP+Road,+Dollar+Scheme+Colony,+Bengaluru,+Karnataka,+India/' + ordersData[id].deliveryArea.latitude + ',' + ordersData[id].deliveryArea.longitude;
        $target.append('<div class="google-maps-directions"><h5><a href="' + directionsUrl + '" target="_blank">Directions in Google Maps</a></h5></div>');
    }

    function showDeliveryLocation() {
        console.log('Will show delivery location');
        setTimeout(function() {
            showDeliveryLocationDelayed(3);
        }, 100);
    }

    function setClickEvent() {
        var $li = $('.table-view-container li.order-card');
        if (!$li.length) {
            // Set event on click after some time because the list is not ready immediately after load event
            setTimeout(setClickEvent, 100);
            return;
        }
        $li.click(function() {
            showDeliveryLocation();
        });
        showDeliveryLocation();
    }

    $(window).on('hashchange', function() {
        setClickEvent();
    });

    setClickEvent();
});

